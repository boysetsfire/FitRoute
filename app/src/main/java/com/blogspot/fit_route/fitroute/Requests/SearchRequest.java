package com.blogspot.fit_route.fitroute.Requests;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Request to the server to search a query
 */
public class SearchRequest extends StringRequest {

    public static final int SUCCESS = 1;
    public static final int INVALID_SEARCH_TERM = -2;
    public static final int NO_CONNECTION = -1;

    private static final String GET_TRACK_INFO_URL = "http://fitroute.ddns.net/search.php";
    private Map<String,String> mParams;

    /**
     * constructor
     * @param query the query to be searched
     * @param listener
     * @param errorListener
     */
    public SearchRequest(String query, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Request.Method.POST, GET_TRACK_INFO_URL, listener, errorListener);
        mParams = new HashMap<>();
        mParams.put("searchTerm",query);
    }

    @Override
    public Map<String, String> getParams() {
        return mParams;
    }
}
