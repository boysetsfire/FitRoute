package com.blogspot.fit_route.fitroute.Requests;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;


/**
 * Request to the server to save a track
 */
public class SaveTrackRequest extends StringRequest {

    //reponse encodings
    public static final int SUCCESS = 1;
    public static final int CONNECTION_ERROR = -1;
    public static final int PARAMETER_MISSING = -3;
    public static final int NO_DATA = -4;

    private static final String SAVE_TRACK_URL = "http://fitroute.ddns.net/writeTrack.php";
    private byte[] mPointsBytes;

    /**
     * constructor
     * @param pointsBytes the track encoded as byte array
     * @param listener
     * @param errorListener
     */
    public SaveTrackRequest(byte[] pointsBytes,Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, SAVE_TRACK_URL, listener, errorListener);
        mPointsBytes = pointsBytes;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        return mPointsBytes;
    }

    @Override
    public String getBodyContentType() {
        return "application/octet-stream; charset=utf-8";
    }
}
