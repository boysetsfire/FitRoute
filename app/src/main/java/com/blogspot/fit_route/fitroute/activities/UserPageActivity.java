package com.blogspot.fit_route.fitroute.activities;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.blogspot.fit_route.fitroute.R;
import com.blogspot.fit_route.fitroute.Requests.GetTrackInfoRequest;
import com.blogspot.fit_route.fitroute.Requests.GetTrackInfosByUserRequest;
import com.blogspot.fit_route.fitroute.services.FitRouteArrayAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * User Page
 */
public class UserPageActivity extends AbstractFitRouteActivity {


    /**
     * the id of the userpage shown
     */
    private int mUserId;
    /**
     * All the routes the user created
     */
    private ListView mUserListView;
    /**
     * the adapter for the list ListView
     */
    private FitRouteArrayAdapter mAdapter;
    /**
     * username of the user
     */
    private String mUsername;
    /**
     * the view to change the user name displayed
     */
    private TextView mUsernameView;
    /**
     * whether its a new track (directed from newly created track) in which some things (manly back buttons) behave differently
     */
    private boolean mIsNewTrack;

    /**
     * Error listener for the response of the request to get all created tracks
     */
    Response.ErrorListener mErrorListenerTrack = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            System.out.println("da");
            error.getStackTrace();
        }
    };
    /**
     * Respone listener for the response of the request to get all created tracks
     */
    Response.Listener<String> mResponseListenerTrack = new Response.Listener<String>(){
        @Override
        public void onResponse(String response) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                proccessReturnCode(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState,R.layout.activity_user_page);

        mUserId=-1;
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            mUserId = (int)extras.get("id");
            mIsNewTrack = extras.getBoolean("newTrack",false);
        }
        if(mUserId==-1){
            finish();
            return;
        }

        //if its the page of the used himself, change selected item to 'my routes'
        //if not it came from a search and 'search' should be selected
        if(mUserId==mSessionManager.getUserId()){
            setupSwipeMenu(false,2);
        }else{
            setupSwipeMenu(false,1);
        }

        mUsernameView =  findViewById(R.id.usernameUserpage);

        //request trackinfos of the user
        GetTrackInfosByUserRequest getTrackInfosByUserRequest = new GetTrackInfosByUserRequest(mUserId,mResponseListenerTrack,mErrorListenerTrack);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(getTrackInfosByUserRequest);
    }

    /**
     * populate the list of created tracks
     * @param values the tracks the user has created formatted as strings
     */
    private void populateList(String[] values){
        mAdapter = new FitRouteArrayAdapter(this, values);
        mUserListView = findViewById(R.id.listViewUserTracks);
        mUserListView.setAdapter(mAdapter);

        mUserListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
            {
              int clickedId = Integer.parseInt(mUserListView.getAdapter().getItem(position).toString());
              showTrackDetailPage(clickedId);
            }
        });

    }

    /**
     * if the user clicks on a item (so a track) on the list this method gets called and the user gets
     * redirected to the linked track
     * @param clickedId
     */
    private void showTrackDetailPage(int clickedId){
        Intent intent = new Intent(UserPageActivity.this,RouteDetailPageActivity.class);
        intent.putExtra("id",clickedId);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * decode the parameters from the answer
     * @param jsonObject the jsonObject which has the track infos as a json object
     * @return the decoded parameters in a string array
     * @throws JSONException if the jsonobject cant be decoded
     */
    private String[] decodeParams(JSONObject jsonObject) throws JSONException {
        Iterator<?> keys = jsonObject.keys();
        List<String> tracksList = new ArrayList<>();
        while( keys.hasNext() ) {
            String key = (String) keys.next();
            if (jsonObject.get(key) instanceof JSONObject) {
                JSONObject object = (JSONObject)jsonObject.get(key);
                tracksList.add((String)object.get("data"));
            }
        }
        String[] returnArray = new String[tracksList.size()];
        returnArray = tracksList.toArray(returnArray);
        return returnArray;
    }

    /**
     * set the username to display in the correct way
     * @param username
     */
    private void setUsername(String username){
        if(username.endsWith("s")){
            mUsernameView.setText(username+"' Routes");
        }else{
            mUsernameView.setText(username+"'s Routes");
        }
    }

    /**
     * handle clicks on the back button on the top left
     * @param view
     */
    public void handleBackButtonClickUserPage(View view){
        if(!mIsNewTrack){
            this.finish();
        }else{
            startActivity(TrackActivity.class);
        }
        this.overridePendingTransition(android.R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onBackPressed() {
        if(!mIsNewTrack){
            super.onBackPressed();
        }else{
            startActivity(TrackActivity.class);
        }
        this.overridePendingTransition(android.R.anim.slide_in_left, R.anim.slide_out_right);
    }

    /**
     * proccess return code of fetched data
     * @param jsonObject the jsonobject to be checked
     * @throws JSONException
     */
    private void proccessReturnCode(JSONObject jsonObject) throws JSONException {

        int returnCode = jsonObject.getInt("success");

        if(returnCode== GetTrackInfosByUserRequest.SUCCESS){
             mUsername = (String)jsonObject.get("username");
            setUsername(mUsername);
            jsonObject.remove("success");
            jsonObject.remove("username");
            String[] decoded = decodeParams(jsonObject);
            populateList(decoded);
            return;
        }
        String message;
        switch(returnCode){
            case GetTrackInfosByUserRequest.PARAMETER_MISSING: message=getString(R.string.unknown_error);break;
            case GetTrackInfosByUserRequest.TRACK_NOT_FOUND: message=getString(R.string.unknown_error);break;
            default: message=getString(R.string.unknown_error);
        }
        this.showErrorMessage(message);
    }

    @Override
    protected void handleMyRoutesButtonClick() {
        if(mUserId==mSessionManager.getUserId()){
            //nothing if we are already on the page of the user
        }else{
            super.handleMyRoutesButtonClick(); //if it came from a search
        }
    }

    @Override
    protected void handleSearchButtonClick() {
        if(mUserId!=mSessionManager.getUserId()){
            //nothing if its not the page of the user, because than it came from a search
        }else{
            super.handleSearchButtonClick();
        }
    }
}