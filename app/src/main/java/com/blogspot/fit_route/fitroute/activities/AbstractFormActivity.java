package com.blogspot.fit_route.fitroute.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.blogspot.fit_route.fitroute.R;

public abstract class AbstractFormActivity extends AbstractFitRouteActivity {

    /**
     * To save wheter the activty is the entry point
     */
    protected boolean mIsEntryPoint;

    /**
     * used for result intent, random number no reason
     */
    protected final int CHILD_ACTIVITY = 1337;

    /**
     * to check wheter the activity is the entry point
     * @return true if the activity is the entry point, false if not
     */
    public boolean isEntryPoint() {
        //if the entry extra exists and is true, the called activty is the entry point, hence the var needs to be false
        if (this.getIntent().getExtras() != null) {
            return !(getIntent().getExtras().getBoolean("entry"));
        }
        return true;
    }

    /**
     * handle a click on the back button.
     * @param view the view
     */
    public void handleBackButtonClick(View view) {
        if(!mIsEntryPoint){
            Intent resultIntent = new Intent();
            resultIntent.putExtra("finished", true);
            setResult(android.app.Activity.RESULT_OK, resultIntent);
        }
        this.finish();
        this.overridePendingTransition(android.R.anim.slide_in_left, R.anim.slide_out_right);
        return;
    }

    /**
     * the result.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHILD_ACTIVITY) {
            if (resultCode == RESULT_OK) { //if back button is clicked, close parent aswell
                if (data.getExtras().getBoolean("finished")) {
                    finish();
                }
            }
        }

    }
}
