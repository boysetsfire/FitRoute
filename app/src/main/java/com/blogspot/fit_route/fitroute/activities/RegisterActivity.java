package com.blogspot.fit_route.fitroute.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.blogspot.fit_route.fitroute.R;
import com.blogspot.fit_route.fitroute.services.MD5Util;
import com.blogspot.fit_route.fitroute.Requests.RegisterRequest;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Activity for register
 */
public class RegisterActivity extends AbstractFormActivity {

    /**
     * the entered name
     */
    EditText mTName;
    /**
     * the entered password
     */
    EditText mTPassword;
    /**
     * the entered email
     */
    EditText mTEmail;

    /**
     * erorr listener for register request to server
     */
    Response.ErrorListener mErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            showErrorMessage(getString(R.string.undefined_error));
            error.getStackTrace();
        }
    };

    /**
     * respone listener for register request to server
     */
    Response.Listener<String> mResponseListener = new Response.Listener<String>(){
        @Override
        public void onResponse(String response) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                proccessReturnCode(jsonObject.getInt("success"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState,R.layout.activity_register);
        mTName =  findViewById(R.id.NameText);
        mTPassword =  findViewById(R.id.PasswordText);
        mTEmail =  findViewById(R.id.EmailText);
        mIsEntryPoint = this.isEntryPoint();
    }


    /**
     * handle click on the register button and send a register request, if fields are valid
     * @param view
     */
    public void handleRegisterButtonClick(View view){
        final String name = mTName.getText().toString();
        final String password = MD5Util.hash(mTPassword.getText().toString());
        final String email = mTEmail.getText().toString();

        if(!isInputNotEmpty()) {
            showErrorMessage(getString(R.string.parameter_missing));
            return;
        }
        if(!isEmailValid()){
            showErrorMessage(getString(R.string.email_not_valid));
            return;
        }
        RegisterRequest registerRequest = new RegisterRequest(name,password,email,mResponseListener,mErrorListener);
        Volley.newRequestQueue(RegisterActivity.this).add(registerRequest);
    }

    /**
     * check if the userinput is not empty
     * @return
     */
    private boolean isInputNotEmpty(){
        return (mTName != null && !mTName.getText().toString().equals("")) &&
                (mTPassword != null && !mTPassword.getText().toString().equals("")) &&
                (mTEmail != null && !mTEmail.getText().toString().equals(""));
    }

    /**
     * check if the entered email is valid
     * @return
     */
    private boolean isEmailValid(){
        return Patterns.EMAIL_ADDRESS.matcher(mTEmail.getText().toString()).matches();
    }

    /**
     * processes the return code from the server
     * @param returnCode the return code the server sent
     */
    private void proccessReturnCode(int returnCode){
        if(returnCode==RegisterRequest.SUCCESS){
            closeActivity();
            return;
        }
        String message = "";
        switch(returnCode){
            case RegisterRequest.USERNAME_ALREADY_EXISTS: message=getString(R.string.username_already_exists);break;
            case RegisterRequest.CONNECTION_ERROR: message=getString(R.string.connection_error);break;
            case RegisterRequest.PARAMETER_MISSING: message=getString(R.string.parameter_missing);break;
            case RegisterRequest.UNDEFINED_ERROR: message=getString(R.string.undefined_error);break;
            default: message=getString(R.string.undefined_error);
        }
        this.showErrorMessage(message);
    }

    /**
     * if user clicks on the login button
     * @param view
     */
    public void handleLoginButtonClick(View view) {
        this.closeActivity();
    }

    /**
     * if activity is closed, checks if its the entry point to be sure to close both register and login
     * activities of both are opened
     */
    private void closeActivity(){
        if (!mIsEntryPoint) { // if its not the entry point just finish to expose register
            this.finish();
            return;
        }
        final Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("entry", true);
        startActivityForResult(intent, CHILD_ACTIVITY);
    }

}
