package com.blogspot.fit_route.fitroute.Requests;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Request to server to login user
 */
public class LoginRequest extends StringRequest{


    private static final String LOGIN_REQUEST_URL = "http://fitroute.ddns.net/login.php";
    private Map<String,String> params;

    //reponse encodings
    public static final int LOGIN_SUCCESS = 1;
    public static final int LOGIN_CONNECTION_ERROR = -1;
    public static final int WRONG_CREDENTIALS = -2;
    public static final int PARAMETER_MISSING = -3;
    public static final int UNDEFINED_ERROR = -4;


    /**
     * constructor
     * @param name name of the user
     * @param password password of the user (MD5 encoded)
     * @param listener
     * @param errorListener
     */
    public LoginRequest(String name, String password, Response.Listener<String> listener, Response.ErrorListener errorListener){
        super(Method.POST,LOGIN_REQUEST_URL,listener,errorListener);
        params = new HashMap<>();
        if(name!=null && !name.equals(""))params.put("name",name);
        if(password!=null && !password.equals(""))params.put("password",password);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }

}
