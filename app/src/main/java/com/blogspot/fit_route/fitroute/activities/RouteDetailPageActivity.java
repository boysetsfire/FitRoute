package com.blogspot.fit_route.fitroute.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.blogspot.fit_route.fitroute.R;
import com.blogspot.fit_route.fitroute.Requests.GetTrackInfoRequest;
import com.blogspot.fit_route.fitroute.Requests.GetTrackRequest;
import com.blogspot.fit_route.fitroute.services.DataFormatter;
import com.blogspot.fit_route.fitroute.services.TrackByteConverter;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Detail page for Tracks
 */
public class RouteDetailPageActivity extends AbstractFitRouteActivity implements OnMapReadyCallback {


    /**
     * MagFragement for google maps
     */
    private SupportMapFragment mMapFragment;
    /**
     * google map reference
     */
    private GoogleMap mMap;
    /**
     * polyline of track
     */
    private Polyline mGpsData;
    /**
     * is mapped is already zoomed to the users track
     */
    private boolean mapZoomed;
    /**
     * if map finished loading
     */
    private boolean mapLoaded;

    /**
     * total distance of the track
     */
    private int mTotalDistance;
    /**
     * total distance of track
     */
    private int mTotalTime;
    /**
     * note of the track
     */
    private String mNoteInput;
    /**
     * id of the track
     */
    private int mTrackId;
    /**
     * name of the one who created the track
     */
    private String mOwnerName;
    /**
     * name of the city the track was taken in
     */
    private String mCity;
    /**
     * type -> codes 0-5
     */
    private int mType;
    /**
     * rating -> code 0-4
     */
    private int mRating;

    /**
     * id of the track owner
     */
    private int mUid;

    /**
     * list of points
     */
    private ArrayList<LatLng> mPoints;
    /**
     * if the track is newly created
     */
    private boolean mNewTrack;

    /**
     * Textview for the username
     */
    private TextView mUsername;
    /**
     * Textview for the duration
     */
    private TextView mDuration;
    /**
     * Textview for the duration subtext
     */
    private TextView mDurationSubText;
    /**
     * Textview for the distance
     */
    private TextView mDistance;
    /**
     * Textview for the note
     */
    private TextView mNote;
    /**
     * ImageView for the rating image
     */
    private ImageView mRatingImage;
    /**
     * Imageview for the type image
     */
    private ImageView mTypeImage;

    /**
     * Response listener, if the track needs to be fetched from server (for info)
     */
    Response.Listener<String> mResponseListenerTrackInfo = new Response.Listener<String>(){
        @Override
        public void onResponse(String response) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                proccessReturnCode(jsonObject);
                showTrackOnScreen();
                loadTrackData();
            } catch (JSONException e) {
                showErrorMessage(getString(R.string.undefined_error));
                e.printStackTrace();
            }
        }
    };
    /**
     * Error listener, if the track needs to be fetched from server (for info)
     */
    Response.ErrorListener mErrorListenerTrackInfo = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            error.getStackTrace();
            showErrorMessage(getString(R.string.undefined_error));
        }
    };

    /**
     *
     * Response listener, if the track needs to be fetched from server (for Track itself)
     */
    Response.Listener<String> mResponseListenerTrack = new Response.Listener<String>(){
        @Override
        public void onResponse(String response) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if((Integer)jsonObject.get("success")==1){
                    byte[] daten = Base64.decode((String)jsonObject.get("trackId"),Base64.DEFAULT);
                    mPoints = TrackByteConverter.getListFromBytes(daten);
                    if(!mapZoomed && mPoints!=null && mapLoaded){
                        zoomMap();
                    }
                }else{
                    showErrorMessage(getString(R.string.undefined_error));
                }
            } catch (JSONException e) {
                showErrorMessage(getString(R.string.undefined_error));
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    };
    /**
     * error listener, if the track needs to be fetched from server (for Track itself)
     */
    Response.ErrorListener mErrorListenerTrack = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            error.getStackTrace();
            showErrorMessage(getString(R.string.undefined_error));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState,R.layout.activity_route_detail_page);
        mapLoaded=false;
        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapDetail);
        mMapFragment.getMapAsync(this);
        mapZoomed=false;
        mNewTrack=false;

        mUsername = findViewById(R.id.userNameDetail);
        mDuration = findViewById(R.id.durationTextDetail);
        mDurationSubText = findViewById(R.id.durationDetailBottomText);
        mDistance = findViewById(R.id.distanceTextDetail);
        mNote = findViewById(R.id.noteDetail);
        mRatingImage = findViewById(R.id.ratingDetail);
        mTypeImage = findViewById(R.id.typeDetail);
        deactivateSwipeMenu();
        getTrackToDisplay();
    }

    /**
     * To check whether the track is in memory of app or if it has to be fetched from server
     */
    private void getTrackToDisplay(){
        mTotalDistance=-1;
        mTotalTime=-1;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            try{ //check if all parameters are given to load track without requesting it
                mNewTrack = extras.getBoolean("newTrack");
                mTotalDistance = (int)extras.get("distance");
                mTotalTime = (int)extras.get("time");
                mNoteInput = (String)extras.get("note");
                mType = (int)extras.get("type");
                mRating = (int)extras.get("rating");
                mUid = (int)extras.get("uId");
                mPoints = getIntent().getParcelableArrayListExtra("locations");
                mOwnerName = mSessionManager.getUserName();
            }catch(NullPointerException e){ }
        }
        if(mTotalDistance!=-1 &&  mTotalTime!=-1 && mPoints!=null && mNoteInput!=null && mOwnerName!=null){
            showTrackOnScreen(); //we got all parameters we need -> track in memory
            if(!mapZoomed && mPoints!=null && mapLoaded){
                zoomMap(); //zoom map if map is ready
            }
            return;
        }
        //if track is not given, at least the id of the to be loaded track should be
        try{
            Integer id = (int)extras.get("id");
            GetTrackInfoRequest getTrackInfoRequest = new GetTrackInfoRequest(id,mResponseListenerTrackInfo,mErrorListenerTrackInfo);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(getTrackInfoRequest);
        }catch (NullPointerException ex){
            showErrorMessage(getString(R.string.no_id_given));
            startActivity(StartActivity.class);
        }catch (ClassCastException ex){
            showErrorMessage(getString(R.string.wrong_parameters));
            startActivity(StartActivity.class);
        }
    }

    /**
     * To synchronize the values of the variables with the view
     */
    private void showTrackOnScreen(){
        mDuration.setText(DataFormatter.getDurationFormatted(mTotalTime));
        if(mTotalTime<3600){
            mDurationSubText.setText(getString(R.string.duration_min_sec));
        }else{
            mDurationSubText.setText(getString(R.string.duration_hours_min_sec));
        }
        mDistance.setText(DataFormatter.getDistanceFormatted(mTotalDistance));

        if(mNoteInput!=null && !mNoteInput.equals("")){
            mNote.setText("\""+mNoteInput+"\"");
        }else{
            mNote.setText("no note!");
        }
        mUsername.setText(mOwnerName);
        //mRatingImage = findViewById(R.id.ratingDetail);
        switch (mType){
            case 0: mTypeImage.setImageResource(R.drawable.ic_running);break;
            case 1: mTypeImage.setImageResource(R.drawable.ic_bike);break;
            case 2: mTypeImage.setImageResource(R.drawable.ic_car);break;
            case 3: mTypeImage.setImageResource(R.drawable.ic_motor);break;
            case 4: mTypeImage.setImageResource(R.drawable.ic_other);break;
        }

        switch (mRating){
            case 0: mRatingImage.setImageResource(R.drawable.ic_0sterne);break;
            case 1: mRatingImage.setImageResource(R.drawable.ic_1sterne);break;
            case 2: mRatingImage.setImageResource(R.drawable.ic_2sterne);break;
            case 3: mRatingImage.setImageResource(R.drawable.ic_3sterne);break;
        }

    }

    /**
     * load track data, only to be called if the track id is saved in mTrackid
     */
    private void loadTrackData(){
        GetTrackRequest getTrackRequest = new GetTrackRequest(mTrackId,mResponseListenerTrack,mErrorListenerTrack);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(getTrackRequest);
    }


    /**
     * handle click on the back button on top left
     * @param view
     */
    public void handleBackButtonClick(View view){
        if(mNewTrack){
            Intent intent = new Intent(RouteDetailPageActivity.this,UserPageActivity.class);
            intent.putExtra("id",mUid);
            intent.putExtra("newTrack",true);
            startActivity(intent);
        }else{
            this.finish();
        }
        this.overridePendingTransition(android.R.anim.slide_in_left, R.anim.slide_out_right);
    }

    /**
     * proccess return code of fetched data
     * @param jsonObject the jsonobject to be checked
     * @throws JSONException
     */
    private void proccessReturnCode(JSONObject jsonObject) throws JSONException {

        int returnCode = jsonObject.getInt("success");

        if(returnCode== GetTrackInfoRequest.SUCCESS){
            mTotalDistance = jsonObject.getInt("distance");
            mTotalTime = jsonObject.getInt("duration");
            mNoteInput = jsonObject.getString("note");
            mCity = jsonObject.getString("city");
            mTrackId = jsonObject.getInt("routeTrackId");
            mType = jsonObject.getInt("type");
            mRating = jsonObject.getInt("rating");
            mOwnerName = jsonObject.getString("ownerName");
            mUid = jsonObject.getInt("uId");
            return;
        }
        String message;
        switch(returnCode){
            case GetTrackInfoRequest.PARAMETER_MISSING: message=getString(R.string.unknown_error);break;
            case GetTrackInfoRequest.TRACK_NOT_FOUND: message=getString(R.string.unknown_error);break;
            default: message=getString(R.string.unknown_error);
        }
        this.showErrorMessage(message);
    }

    /**
     * zoom data to polyline (if given)
     */
    private void zoomMap(){
        mapZoomed=true;
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.color(Color.CYAN);
        polylineOptions.width(4);
        mGpsData = mMap.addPolyline(polylineOptions);
        mGpsData.setPoints(mPoints);
        centerMap();
    }

    /**
     * center map
     */
    private void centerMap(){
        LatLngBounds.Builder boundsBuilder = LatLngBounds.builder();
        for(LatLng point : mPoints){
            boundsBuilder.include(point);
        }
        LatLngBounds bounds = boundsBuilder.build();
        final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 50);
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.animateCamera(cameraUpdate);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        mapLoaded=true;
        if(!mapZoomed && mPoints!=null){
            zoomMap();
        }
    }

    @Override
    public void onBackPressed() {
        if(!mNewTrack){
            super.onBackPressed();
            this.overridePendingTransition(android.R.anim.slide_in_left, R.anim.slide_out_right);
        }
    }

}
