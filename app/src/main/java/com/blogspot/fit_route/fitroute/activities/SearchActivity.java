package com.blogspot.fit_route.fitroute.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.blogspot.fit_route.fitroute.R;
import com.blogspot.fit_route.fitroute.Requests.SearchRequest;
import com.blogspot.fit_route.fitroute.services.FitRouteSearchAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Activity for searching the database.
 * Search returns either users or tracks (or both)
 */
public class SearchActivity extends AbstractFitRouteActivity {

    /**
     * the adapter for the result list
     */
    private FitRouteSearchAdapter mAdapter;
    /**
     * the search result list
     */
    private ListView mResultView;

    /**
     * errorlistener for search results
     */
    Response.ErrorListener mErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            showErrorMessage(getString(R.string.undefined_error));
        }
    };

    /**
     * Respone listener for the search results
     */
    Response.Listener<String> mResponseListener = new Response.Listener<String>(){
        @Override
        public void onResponse(String response) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                proccessReturnCode(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState,R.layout.activity_search);
        setupSwipeMenu(false,1);

        SearchView searchView = findViewById(R.id.searchView);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                processSearchText(query);
                return false;
            }
            @Override public boolean onQueryTextChange(String newText) { return false; }
        });
    }

    /**
     * if user has entered search this method gets called, which sends a request to the server
     * @param query
     */
    private void processSearchText(String query){
        SearchRequest getTrackInfosByUserRequest = new SearchRequest(query,mResponseListener,mErrorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(getTrackInfosByUserRequest);

    }

    /**
     * if the results for the server are aviable, this method gets called to populate the result list
     * @param values
     */
    private void populateList(String[] values){
        mAdapter = new FitRouteSearchAdapter(this, values);
        mResultView = findViewById(R.id.listViewUserTracks);
        mResultView.setAdapter(mAdapter);

        mResultView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            { ;
                int itemId = Integer.parseInt(mResultView.getAdapter().getItem(position).toString());
                if(mAdapter.getType(position).equals("track")){
                    showTrackDetailPage(itemId);
                }else{
                    showUserPage(itemId);
                }


            }
        });
    }

    /**
     * handle click on the back button on the top left
     * @param view
     */
    public void handleBackButtonClicked(View view){
        this.finish();
        this.overridePendingTransition(android.R.anim.slide_in_left, R.anim.slide_out_right);
    }

    /**
     * if user clicks on a row in the result list and it is a track this method gets called and show the page of that track
     * @param id id of the track to be shown
     */
    private void showTrackDetailPage(int id){
        Intent intent = new Intent(SearchActivity.this,RouteDetailPageActivity.class);
        intent.putExtra("id",id);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    /**
     * if user clicks on a row in the result list and it is a user this method gets called and show the page of that user
     * @param id id of the user to be shown
     */
    private void showUserPage(int id){
        Intent intent = new Intent(SearchActivity.this,UserPageActivity.class);
        intent.putExtra("id",id);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    /**
     * decode the parameters from the answer
     * @param jsonObject the jsonObject which has the track infos as a json object
     * @return the decoded parameters in a string array
     * @throws JSONException if the jsonobject cant be decoded
     */
    private String[] decodeParams(JSONObject jsonObject) throws JSONException {
        Iterator<?> keys = jsonObject.keys();
        List<String> tracksList = new ArrayList<>();
        while( keys.hasNext() ) {
            String key = (String) keys.next();
            if (jsonObject.get(key) instanceof JSONObject) {
                JSONObject object = (JSONObject)jsonObject.get(key);
                tracksList.add((String)object.get("data"));
            }
        }
        String[] returnArray = new String[tracksList.size()];
        returnArray = tracksList.toArray(returnArray);
        return returnArray;
    }

    /**
     * proccess return code of fetched data
     * @param jsonObject the jsonobject to be checked
     * @throws JSONException
     */
    private void proccessReturnCode(JSONObject jsonObject) throws JSONException {

        int returnCode = jsonObject.getInt("success");

        if(returnCode== SearchRequest.SUCCESS){
            jsonObject.remove("success");
            String[] decoded = decodeParams(jsonObject);
            populateList(decoded);
            return;
        }
        String message;
        switch(returnCode){
            case SearchRequest.INVALID_SEARCH_TERM: message=getString(R.string.invalid_search);break;
            case SearchRequest.NO_CONNECTION: message=getString(R.string.connection_error);break;
            default: message=getString(R.string.unknown_error);
        }
        this.showErrorMessage(message);
    }

    @Override
    protected void handleSearchButtonClick(){/*Do not do anything, so multiple clicks on 'search' dont get recognized*/}

}
