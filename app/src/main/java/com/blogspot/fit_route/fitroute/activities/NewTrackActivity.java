package com.blogspot.fit_route.fitroute.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.blogspot.fit_route.fitroute.R;
import com.blogspot.fit_route.fitroute.Requests.SaveTrackInfoRequest;
import com.blogspot.fit_route.fitroute.Requests.SaveTrackRequest;
import com.blogspot.fit_route.fitroute.services.DataFormatter;
import com.blogspot.fit_route.fitroute.services.TrackByteConverter;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


/**
 * Activity for a new track
 */
public class NewTrackActivity extends AbstractFitRouteActivity implements OnMapReadyCallback {


    /**
     * Map Fragement
     */
    private SupportMapFragment mMapFragment;
    /**
     * Google maps to show track
     */
    private GoogleMap mMap;
    /**
     * Gps data of the new track
     */
    private Polyline mGpsData;
    /**
     * points of the gps data
     */
    private ArrayList<LatLng> mPoints;
    /**
     * total distance of the track
     */
    private int mTotalDistance;
    /**
     * total time of the track
     */
    private int mTotalTime;
    /**
     * location in which the track was recorded
     */
    private String mLocation;
    /**
     * note input of the user
     */
    private String mNoteInput;
    /**
     * textview to show duration
     */
    private TextView mDurationTextDone;
    /**
     * textview to show distance
     */
    private TextView mDistanceTextDone;
    /**
     * textview for the bottomtext of duration
     */
    private TextView mDurationDoneBottomText;
    /**
     * radio button to select type of track
     */
    private RadioGroup mTypeRadioGroup;
    /**
     * radio button to rate track
     */
    private RadioGroup mRatingRadioGroup;
    /**
     * button to add a note
     */
    private Button mAddNoteButton;


    /**
     * Listener for saving track
     */
    Response.ErrorListener mErrorListenerTrack = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            showErrorMessage(getString(R.string.connection_error));
            error.getStackTrace();
        }
    };
    /**
     * errorlistener for saving track
     */
    Response.Listener<String> mResponseListenerTrack = new Response.Listener<String>(){
        @Override
        public void onResponse(String response) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                proccessReturnCodeTrack(jsonObject.getInt("success"),jsonObject.getInt("id"));
            } catch (JSONException e) {
                showErrorMessage(getString(R.string.undefined_error));
                e.printStackTrace();
            }
        }
    };

    /**
     * listener for track info save
     */
    Response.ErrorListener getmErrorListenerTrackInfo = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            showErrorMessage(getString(R.string.connection_error));
            error.getStackTrace();
        }
    };
    /**
     * error listener for track info save
     */
    Response.Listener<String> mResponseListenerTrackInfo = new Response.Listener<String>(){
        @Override
        public void onResponse(String response) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                proccessReturnCodeTrackInfo(jsonObject.getInt("success"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState,R.layout.activity_new_track);
        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
        if(getIntent().getExtras()!=null){
            mTotalDistance = (int)getIntent().getExtras().get("distance");
            mTotalTime = (int)getIntent().getExtras().get("time");
            mPoints = getIntent().getParcelableArrayListExtra("locations");
            mLocation = (String)getIntent().getExtras().get("location");
        }
        mNoteInput="";
        mDistanceTextDone = findViewById(R.id.distanceTextDone);
        mDurationTextDone = findViewById(R.id.durationTextDone);
        mDurationDoneBottomText = findViewById(R.id.durationDoneBottomText);
        mAddNoteButton = findViewById(R.id.addNoteButton);
        mTypeRadioGroup = findViewById(R.id.typeRadioGroup);
        mRatingRadioGroup = findViewById(R.id.ratingRadioGroup);
        super.deactivateSwipeMenu();
        setupView();
    }

    /**
     * setup the view of the track
     */
    private void setupView(){
        mDistanceTextDone.setText(DataFormatter.getDistanceFormatted(mTotalDistance));
        mDurationTextDone.setText(DataFormatter.getDurationFormatted(mTotalTime));
        if(mTotalTime>3600){
            mDurationDoneBottomText.setText(getString(R.string.duration_hours_min_sec));
        }else{
            mDurationDoneBottomText.setText(getString(R.string.duration_min_sec));
        }
    }

    /**
     * if the user clicks on 'add note' this method fires
     * @param view
     */
    public void handleAddNoteClick(View view){
        showInputDialog();
    }

    /**
     * if the user clicks on done to save the track
     * @param view
     */
    public void handleDoneClick(View view){
        try {
            final byte[] pointsBytes = TrackByteConverter.convertToBytes(mPoints);
            byte[] encodedBytes = Base64.encode(pointsBytes,Base64.DEFAULT);
            SaveTrackRequest saveTrackRequest = new SaveTrackRequest(encodedBytes,mResponseListenerTrack,mErrorListenerTrack);
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(saveTrackRequest);
        } catch (IOException e) {
            showErrorMessage(getString(R.string.undefined_error));
            e.printStackTrace();
        }
    }

    /**
     *  to save the track, after track info got saved
     * @param insertedId id of the track info to the track
     */
    private void insertRouteInfo(int insertedId){
        System.out.println(mSessionManager.getUserId());
        SaveTrackInfoRequest saveTrackInfoRequestRequest = new SaveTrackInfoRequest(mSessionManager.getUserId(),
                insertedId,mLocation,mTotalTime,mTotalDistance,getSelectedIndex(mTypeRadioGroup),
                getSelectedIndex(mRatingRadioGroup),mNoteInput,mResponseListenerTrackInfo,getmErrorListenerTrackInfo);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(saveTrackInfoRequestRequest);
    }

    /**
     * get Selected index of a radio group
     * @param radioGroup the radio group
     * @return the selected index
     */
    private int getSelectedIndex(RadioGroup radioGroup){
        View radioButton = radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
        return radioGroup.indexOfChild(radioButton);
    }


    /**
     * if the saving of both the track itself and track info was successful
     */
    private void savingSuccessful(){
        Intent intent = new Intent(NewTrackActivity.this,RouteDetailPageActivity.class);
        intent.putExtra("distance",mTotalDistance);
        intent.putExtra("time",mTotalTime);
        intent.putExtra("note",mNoteInput);
        intent.putExtra("type",getSelectedIndex(mTypeRadioGroup));
        intent.putExtra("rating",getSelectedIndex(mRatingRadioGroup));
        intent.putExtra("newTrack",true);
        intent.putExtra("uId",mSessionManager.getUserId());
        intent.putParcelableArrayListExtra("locations",mPoints);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * show input dialog if user clicks on add note
     */
    private void showInputDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.add_note_heading));

        // Set up the input
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(mNoteInput);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton(getString(R.string.okay), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mNoteInput = input.getText().toString();
                updateAddNoteText();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        //open keyboard
        AlertDialog dialog = builder.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
    }

    /**
     * if a note is inputet the text of the add note changes from 'add note' to 'edit note' and vise versa
     */
    private void updateAddNoteText(){
        if(mNoteInput.equals("") || mNoteInput==null){
            mAddNoteButton.setText(getString(R.string.add_a_note));
        }else{
            mAddNoteButton.setText(getString(R.string.edit_note));
        }
    }

    /**
     * proccesses the processcode of the return from server after saving track
     * @param returnCode
     * @param insertedId
     */
    private void proccessReturnCodeTrack(int returnCode,int insertedId){
        if(returnCode== SaveTrackRequest.SUCCESS){
            insertRouteInfo(insertedId);
            return;
        }
        String message = "";
        switch(returnCode){
            case SaveTrackRequest.CONNECTION_ERROR: message=getString(R.string.connection_error);break;
            case SaveTrackRequest.PARAMETER_MISSING: message=getString(R.string.faulty_track);break;
            case SaveTrackRequest.NO_DATA: message=getString(R.string.undefined_error);break;
            default: message=getString(R.string.undefined_error);
        }
        this.showErrorMessage(message);
    }
    /**
     * proccesses the processcode of the return from server after saving track info
     * @param returnCode
     * @param returnCode
     */
    private void proccessReturnCodeTrackInfo(int returnCode){
        if(returnCode== SaveTrackInfoRequest.INSERT_SUCCESS){
            savingSuccessful();
            return;
        }
        String message = "";
        switch(returnCode){
            case SaveTrackInfoRequest.REGISTER_CONNECTION_ERROR: message=getString(R.string.connection_error);break;
            case SaveTrackInfoRequest.PARAMETER_MISSING: message=getString(R.string.parameter_missing);break;
            case SaveTrackInfoRequest.UNDEFINED_ERROR: message=getString(R.string.undefined_error);break;
            default: message=getString(R.string.undefined_error);
        }
        this.showErrorMessage(message);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
        PolylineOptions polylineOptions = new PolylineOptions();
        polylineOptions.color(Color.CYAN);
        polylineOptions.width(4);
        mGpsData = mMap.addPolyline(polylineOptions);
        mGpsData.setPoints(mPoints);
        centerMap();
    }

    private void centerMap(){
        LatLngBounds.Builder boundsBuilder = LatLngBounds.builder();
        for(LatLng point : mPoints){
            boundsBuilder.include(point);
        }
        LatLngBounds bounds = boundsBuilder.build();
        final CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 50);
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mMap.animateCamera(cameraUpdate);
            }
        });
    }

    @Override
    public void onBackPressed() {/*once inside the app, going back to startscreen should not ne possible*/}
}
