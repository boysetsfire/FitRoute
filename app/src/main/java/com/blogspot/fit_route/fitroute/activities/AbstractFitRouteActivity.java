package com.blogspot.fit_route.fitroute.activities;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.blogspot.fit_route.fitroute.R;
import com.blogspot.fit_route.fitroute.services.SessionManager;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Abstract class which is the parent of every activity an provides basic functionality
 */
public abstract class AbstractFitRouteActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener  {
    /**
     * The session manager, which saves the data of the user
     */
    SessionManager mSessionManager;

    /**
     * drawer layout, to close the drawer
     */
    private DrawerLayout mDrawerLayout;

    /**
     * OnCreate with modified parameters to set the content view
     * @param savedInstanceState standard
     * @param layout the layout which should be shown
     */
    protected void onCreate(Bundle savedInstanceState,int layout) {
        super.onCreate(savedInstanceState);
        setContentView(layout);
        mSessionManager = new SessionManager(getApplicationContext());
        mDrawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.menu);
        if(navigationView!=null){
            navigationView.setNavigationItemSelectedListener(this);
        }
    }

    /**
     * Sets up the swipe menu if its aviable
     * @param registerButtonVisible should the register button be visible if the user is not logged in?
     * @param activeMenuItem the menu item to be active
     */
    protected void setupSwipeMenu(boolean registerButtonVisible,int activeMenuItem){
        ImageView swipeArrow = findViewById(R.id.swipeArrow);
        DrawerLayout drawerLayout =  findViewById(R.id.drawer_layout);
        //register button if active
        if(registerButtonVisible){
            Button registerButton = findViewById(R.id.register);
            if(mSessionManager.isLoggedIn())registerButton.setVisibility(View.INVISIBLE);
            else registerButton.setVisibility(View.VISIBLE);
        }
        NavigationView navigationView = findViewById(R.id.menu);
        if(mSessionManager.isLoggedIn()){
            //set header heading to username
            TextView userName = navigationView.getHeaderView(0).findViewById(R.id.userName);
            userName.setText(StringUtils.capitalize(mSessionManager.getUserName().toLowerCase()));
            //enable swipe arrow and drawer and make register button invisible
            if(swipeArrow!=null) {
                swipeArrow.setVisibility(View.VISIBLE);
            }
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            //activate first menu item, since its the one when this view gets set up
            this.activateMenuItem(activeMenuItem);
        }else{
            //disable swipe arrow and drawer
            if(swipeArrow!=null) {
            swipeArrow.setVisibility(View.INVISIBLE);
            }
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    /**
     * Deactivate the swipe menu
     */
    protected void deactivateSwipeMenu(){
        ImageView swipeArrow = findViewById(R.id.swipeArrow);
        DrawerLayout drawerLayout =  findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        if(swipeArrow!=null){
            swipeArrow.setVisibility(View.INVISIBLE);
        }

    }

    /**
     * Starts the given Activity. No Animations.
     * @param targetClass Target Activity to start
     */
    protected void startActivity(Class targetClass){
        this.startActivity(targetClass,0,0);
    }

    /**
     * Starts the given Activity. No Animations.
     * @param targetClass Target Activity to start
     */
    protected void startActivity(Class targetClass,boolean startIfExists){
        this.startActivity(targetClass,0,0,startIfExists);
    }

    /**
     * Starts the given Activity, with the given animations.
     * @param targetClass the target class
     * @param enterAnim the enter animation
     * @param exitAnim the exit animation
     */
    protected void startActivity(Class targetClass,int enterAnim, int exitAnim){
        startActivity(targetClass,enterAnim,exitAnim,true);
    }

    /**
     * Starts the given Activity, with the given animations.
     * @param targetClass the target class
     * @param enterAnim the enter animation
     * @param exitAnim the exit animation
     * @param startIfExists wheter a new activity of that type should be started, if one already exists
     */
    protected void startActivity(Class targetClass,int enterAnim, int exitAnim,boolean startIfExists){
        if(startIfExists){
            final Intent intent = new Intent(this,targetClass);
            startActivity(intent);
            this.overridePendingTransition(enterAnim, exitAnim);
        }else{
            final Intent intent = getIntent(getApplicationContext(), targetClass);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
            this.overridePendingTransition(enterAnim, exitAnim);
        }

    }

    private static Intent getIntent(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        return intent;
    }


    /**
     * handle navigation buttons clicks
     * @param item clicked item
     * @return if it was successful
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        boolean success=true;
        switch (item.getItemId()) {
            case  R.id.start_tracking: handleStartButtonClick();break;
            case R.id.search: handleSearchButtonClick();break;
            case R.id.my_routes: handleMyRoutesButtonClick();break;
            case R.id.policy: handlePolicyButtonClick();break;
            case R.id.logout: handleLogoutButtonClick();break;
            default:  success=false;
        }
        //close drawer if selection was successful
        if(success) mDrawerLayout.closeDrawers();
        return success;
    }

    /**
     * handle start button on navigation
     */
    protected void handleStartButtonClick(){
        this.startActivity(TrackActivity.class);
        this.activateMenuItem(0);
    }
    /**
     * handle  search on navigation
     */
    protected void handleSearchButtonClick(){
        Intent intent = new Intent(this,SearchActivity.class);
        intent.putExtra("id",mSessionManager.getUserId());
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        this.activateMenuItem(1);
    }
    /**
     * handle my routes on navigation
     */
    protected void handleMyRoutesButtonClick(){
        Intent intent = new Intent(this,UserPageActivity.class);
        intent.putExtra("id",mSessionManager.getUserId());
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        this.activateMenuItem(2);
    }
    /**
     * handle policy on navigation
     */
    protected void handlePolicyButtonClick(){
        Intent intent = new Intent(this,PolicyActivity.class);
        startActivity(intent);
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        this.activateMenuItem(3);
    }
    /**
     * handle logout on navigation
     */
    protected void handleLogoutButtonClick(){
        mSessionManager.logout();
        this.startActivity(StartActivity.class);
        this.activateMenuItem(3);
    }


    /**
     * show a error message with no listener (just to inform the user)
     * @param message
     */
    public void showErrorMessage(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(message).setNegativeButton(getString(R.string.retry),null).create().show();
    }

    /**
     * get current context (active activity)
     * @return
     */
    public Context getContext(){
        return this;
    }

    /**
     * change the selected item in the navigation view
     * @param item
     */
    protected void activateMenuItem(int item){
        ((NavigationView)findViewById(R.id.menu)).getMenu().getItem(item).setChecked(true);
    }


}
