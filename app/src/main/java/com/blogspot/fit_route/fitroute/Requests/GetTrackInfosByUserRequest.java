package com.blogspot.fit_route.fitroute.Requests;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Request to get tracks (just info) of a user by id
 */
public class GetTrackInfosByUserRequest extends StringRequest {

    public static final int SUCCESS = 1;
    public static final int PARAMETER_MISSING = -3;
    public static final int TRACK_NOT_FOUND = -1;


    private static final String GET_TRACK_INFO_URL = "http://fitroute.ddns.net/getTrackInfosByUser.php";
    private Map<String,String> mParams;


    /**
     * constructor
     * @param userId the userid of which we want the tracks of
     * @param listener
     * @param errorListener
     */
    public GetTrackInfosByUserRequest(int userId, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST,GET_TRACK_INFO_URL, listener, errorListener);
        mParams = new HashMap<>();
        mParams.put("id",userId+"");
    }

    @Override
    public Map<String, String> getParams() {
        return mParams;
    }
}
