package com.blogspot.fit_route.fitroute.Requests;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Request to register a user to the server
 */
public class RegisterRequest extends StringRequest {

    private static final String REGISTER_REQUEST_URL = "http://fitroute.ddns.net/register.php";
    private Map<String,String> params;


    //reponse encodings
    public static final int SUCCESS = 1;
    public static final int CONNECTION_ERROR = -1;
    public static final int USERNAME_ALREADY_EXISTS = -2;
    public static final int PARAMETER_MISSING = -3;
    public static final int UNDEFINED_ERROR = -4;


    /**
     * constructor
     * @param name name of the user
     * @param password password of the user (MD5 encypted)
     * @param email email of the user
     * @param listener
     * @param errorListener
     */
    public RegisterRequest(String name,String password,String email, Response.Listener<String> listener,Response.ErrorListener errorListener){
        super(Method.POST,REGISTER_REQUEST_URL,listener,errorListener);
        params = new HashMap<>();
        if(name!=null && !name.equals(""))params.put("name",name);
        if(password!=null && !password.equals(""))params.put("password",password);
        if(email!=null && !email.equals(""))params.put("email",email);
    }


    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
