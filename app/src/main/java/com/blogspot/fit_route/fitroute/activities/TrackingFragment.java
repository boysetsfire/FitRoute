package com.blogspot.fit_route.fitroute.activities;


import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blogspot.fit_route.fitroute.R;
import com.blogspot.fit_route.fitroute.services.DataFormatter;

import org.w3c.dom.Text;

import java.text.DecimalFormat;

/**
 * The Tracking fragement on the bottom of the map when tracking is active
 */
public class TrackingFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tracking, container, false);
    }

    /**
     * updates the speed
     * @param speed the speed in meters per second
     */
    public void updateSpeed(float speed) {
        String speedFormatted = DataFormatter.getSpeedFormatted(speed);
        ((TextView)getView().findViewById(R.id.speed)).setText(speedFormatted);
    }

    /**
     *  updates the disctance
     * @param distance the distance in meters
     */
    public void updateDistance(int distance) {
        String distanceKm = DataFormatter.getDistanceFormatted(distance);
        ((TextView)getView().findViewById(R.id.distance)).setText(distanceKm);
    }

    /**
     * updates the duration
     * @param duration the duration in seconds
     */
    public void updateDuration(int duration) {
        TextView durationText = getView().findViewById(R.id.duration);
        TextView durationBottom = getView().findViewById(R.id.durationBottomText);
        String durationFormatted = DataFormatter.getDurationFormatted(duration);
        durationText.setText(durationFormatted);
        if(duration<3600){
            durationBottom.setText(getString(R.string.duration_min_sec));
        }else{
            durationBottom.setText(getString(R.string.duration_hours_min_sec));
        }


    }

}
