package com.blogspot.fit_route.fitroute.Requests;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Request to get a track by id from the server
 */
public class GetTrackRequest extends StringRequest {

    public static final int SUCCESS = 1;
    public static final int PARAMETER_MISSING = -3;
    public static final int TRACK_NOT_FOUND = -1;


    private static final String GET_TRACK_URL = "http://fitroute.ddns.net/getTrack.php";
    private Map<String,String> mParams;

    /**
     * constructor
     * @param trackId the id of the track whoch should be fetched from
     * @param listener
     * @param errorListener
     */
    public GetTrackRequest(int trackId, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, GET_TRACK_URL, listener, errorListener);
        mParams = new HashMap<>();
        mParams.put("trackId",trackId+"");
    }

    @Override
    public Map<String, String> getParams() {
        return mParams;
    }
}
