package com.blogspot.fit_route.fitroute.services;

import com.google.android.gms.maps.model.LatLng;

/**
 * Communication interface between GpsService and the activity which starts the service
 */
public interface GpsCallbacks {
    /**
     * If new gps data are aviable, this method gets called
     * @param gps the gps data (Lat lng)
     * @param speed the current speed
     * @param speed the current location
     */
    void updateGpsData(LatLng gps,float speed,String location);

    /**
     * if an error occurs, it gets passed through this method
     * @param message the error message
     */
    void errorMessage(String message);

    /**
     * move the camera to the given Location
     * @param location the location
     */
    void moveCamera(LatLng location,float speed);

}
