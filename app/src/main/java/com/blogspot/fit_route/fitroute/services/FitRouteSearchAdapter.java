package com.blogspot.fit_route.fitroute.services;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blogspot.fit_route.fitroute.R;

/**
 * The adapter for search result view
 */
public class FitRouteSearchAdapter  extends ArrayAdapter<String> {

    /**
     * the context in which the search results should be rendered in
     */
    private final Context mContext;
    /**
     * Values of the row.
     * divided up by this schema:
     * if track: 'track';[Type];[Location];[Distance];[DateCreated];[ID]
     * if user: 'user';[regDate];[Name];[Count Routes];[ID]
     */
    private final String[] mValues;

    /**
     * construcor
     * @param context the context
     * @param values the values, correctly formatted
     */
    public FitRouteSearchAdapter(Context context, String[] values) {
        super(context, R.layout.listlayout, values);
        this.mContext = context;
        this.mValues = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        String[] values = mValues[position].split(";");

        //if its a track
        if(values[0].equals("track")){
            View rowView = inflater.inflate(R.layout.listlayout, parent, false);
            ImageView typeView = rowView.findViewById(R.id.typeImage);
            TextView locationView =  rowView.findViewById(R.id.locationText);
            TextView distanceView =  rowView.findViewById(R.id.distanceText);
            TextView dateView =  rowView.findViewById(R.id.dateText);
            //type
            switch (Integer.parseInt(values[1])){
                case 0: typeView.setImageResource(R.drawable.ic_running);break;
                case 1: typeView.setImageResource(R.drawable.ic_bike);break;
                case 2: typeView.setImageResource(R.drawable.ic_car);break;
                case 3: typeView.setImageResource(R.drawable.ic_motor);break;
                case 4: typeView.setImageResource(R.drawable.ic_other);break;
            }
            locationView.setText(values[2]);
            distanceView.setText(DataFormatter.getDistanceFormatted(Integer.parseInt(values[3])));
            dateView.setText(values[4]);
            return rowView;
        }
        //if its a user
        View rowView = inflater.inflate(R.layout.listlayout_user, parent, false);
        TextView usernameView =  rowView.findViewById(R.id.usernameSearch);
        TextView recordedRoutesView =  rowView.findViewById(R.id.recordedRoutesSearch);
        TextView regDateView =  rowView.findViewById(R.id.regDateTextSearch);
        regDateView.setText(values[1]);
        usernameView.setText(values[2]);
        recordedRoutesView.setText(values[3]);
        return rowView;
    }

    @Nullable
    @Override
    public String getItem(int position) {
        String vals[] = mValues[position].split(";");
        if(vals[0].equals("track")){
            return vals[5];
        }
        return vals[4];
    }

    /**
     * returns the type. whether 'track' or 'user' on one position
     * @param position the position
     * @return 'track' or 'user'
     */
    public String getType(int position){
        return  mValues[position].split(";")[0];
    }


}
